package com.fis.tvan.process.base;

public class Const {
    public static final String TAG_JOIN_PARAMS = "@@";

    public static class Language{
        public static final String VN_REQUEST = "vn";
        public static final String EN_REQUEST = "en";
    }

    public static class GroupName{
        public static final String G_REGISTER = "G-REGISTER";
        public static final String G_EINVOICE_HAVE_CODE = "G-EINVOICE-HAVE-CODE";
        public static final String G_EINVOICE_WITHOUT_CODE = "G-EINVOICE-WITHOUT-CODE";
        public static final String G_EINVOICE_SUMMARY = "G-EINVOICE-SUMMARY";

        public static final String[] LIST_GROUP_PARAMS = new String[]{G_REGISTER,
                G_EINVOICE_HAVE_CODE,
                G_EINVOICE_WITHOUT_CODE,
                G_EINVOICE_SUMMARY};

        public static final String[] GROUP_CONFIG_NAME = new String[]{"TVAN_PROCESS"};
    }

    public static class Queue{
        public static final String SCAN_SEND_TAX_QUEUE_NAME = "SEN_TAX";
        public static final String SCAN_RECEIVER_TAX_QUEUE_NAME = "RECEIVER_TAX";
    }

    public static class MessageTmp{
        public static final String XML_TEMPLATE_SEND_TAX_DEFAULT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TDiep><TTChung><PBan>2.0.0</PBan><MNGui>%s</MNGui><MNNhan>TCT</MNNhan><MLTDiep>%s</MLTDiep><MTDiep>%s</MTDiep><MST>%s</MST><SLuong>%d</SLuong></TTChung><DLieu>%s</DLieu></TDiep>";
        public static final String XML_TEMPLATE_CONFIRM_TAX_DEFAULT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><TDiep><TTChung><PBan>2.0.0</PBan><MNGui>%s</MNGui><MNNhan>TCT</MNNhan><MLTDiep>999</MLTDiep><MTDiep>%s</MTDiep><MTDTChieu>%s</MTDTChieu></TTChung><DLieu><TBao><MTDiep>%s</MTDiep><MNGui>%s</MNGui><NNhan>%s</NNhan><TTTNhan>0</TTTNhan></TBao></DLieu></TDiep>";
    }


}
