package com.fis.tvan.process.impl;

import com.fis.tvan.process.base.config.ProviderGlobal;
import mm.common.utils.JsUtils;
import mm.common.utils.JsonObject;

public enum ConfigEnum {

    SER_HANDLE_DELAY_MS,
    SEND_TAX_NUMBER_WORKER,

    EINVOICE_NUMBER_WORKER,
    ;
    public JsonObject value() {
        if(ProviderGlobal.configsApp == null){
            return JsonObject.build(JsUtils.nullVal());
        }
        return ProviderGlobal.configsApp.get(this.toString());
    }
}
