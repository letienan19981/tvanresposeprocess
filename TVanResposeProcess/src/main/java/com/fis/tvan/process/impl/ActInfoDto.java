package com.fis.tvan.process.impl;

public class ActInfoDto<T> {
    public final AppErrorEnum error;
    public final String message;
    public final T data;

    private ActInfoDto(AppErrorEnum error, String message, T data){
        this.error = error;
        this.message = message;
        this.data = data;
    }

    public static <T>ActInfoDto build(AppErrorEnum error, String message, T data){
        return new ActInfoDto(error, message, data);
    }

    public static ActInfoDto build(AppErrorEnum error, String message){
        return new ActInfoDto(error, message, null);
    }

    public static ActInfoDto build(AppErrorEnum error, boolean isEn){
        return new ActInfoDto(error, error.toStr(isEn), null);
    }

}
