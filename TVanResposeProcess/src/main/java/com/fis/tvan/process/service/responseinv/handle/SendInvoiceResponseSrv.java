package com.fis.tvan.process.service.responseinv.handle;

import com.fis.tvan.process.db.DbApp;
import com.fis.tvan.process.impl.ConfigEnum;
import mm.common.utils.GlobalUtils;
import mm.common.utils.HttpClient;
import mm.common.utils.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.github.f4b6a3.ulid.UlidCreator;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class SendInvoiceResponseSrv {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DbApp dbApp;
/*

    private final static int ROW_NUM = 1;

    private int threadId = 0;
    private int runMode = 1; // 1: select by status, 2 select by config time(phục vụ multiple
*/
    private final int MODE_STATUS = 1;
    private final int MODE_TIME = 2;

    /**
     * Khởi tạo và cache các danh mục
     */

   /* public SendInvoiceResponseSrv(int iRunMode, int iThreadId) {
        this.runMode = iRunMode;
        this.threadId = iRunMode == 1 ? iThreadId : 0;
    }*/

    /**
     * Thực hiện gửi dữ liệu cho Einvoice:
     */
    public void sendToInvoice(int iRunMode, int iThreadId, int iRowNum) {
        int sendOk = 1;
        String strErr = "";
        JsonObject jsSend = null;
        JsonObject jsResponse = null;
        Map<String, String> headerInv;
        List<JsonObject> lstMsg;
        JsonObject jsKey = null;
        if (iRunMode == MODE_STATUS) {
            lstMsg = this.dbApp.getSendInvoiceResponseByStatus(iRowNum, 1);
        } else {
            lstMsg = this.dbApp.getSendInvoiceResponse(iThreadId, 1);
        }
        if (null == lstMsg || lstMsg.size() == 0) {
            logger.info("sendToInvoice thread {} - Mode {} empty data!", iThreadId, iRunMode);
        }
        for (JsonObject jsonObject : lstMsg) {
            try {

                jsKey = JsonObject.newObj();
                jsKey.put("f_id_send_invoice", jsonObject.get("f_id"));
                jsKey.put("f_ref_id", jsonObject.get("f_ref_id"));
                jsKey.put("f_ref_id_tvan", jsonObject.get("f_ref_id_tvan"));
                jsKey.put("f_ref_id_tax", jsonObject.get("f_ref_id_tax"));
                jsKey.put("f_tax_payer_code", jsonObject.get("f_tax_payer_code"));

                // build msg theo chuẩn
                jsSend = this.buildMsgSendInvoice(jsonObject);

                // Send invoice
                headerInv = new HashMap<>();
                headerInv.put("Authorization", "Basic " + Base64.getEncoder().encodeToString(
                        (jsonObject.get("f_username").toString() + ":" + jsonObject.get("f_password").toString()
                        ).getBytes()));

                HttpClient.Response req = HttpClient.post(
                        jsonObject.get("f_timeout").toInteger(),
                        jsonObject.get("f_api_endpoint").toString(), headerInv, jsSend.toStringPretty()
                );
                if (!req.isOk()) {
                    sendOk = 4;
                    strErr = "sendToInvoice Err post data";
                } else {
                    jsResponse = req.bodyJs();
                }
                logger.info("sendToInvoice thread {} - Mode {} send invoice fTvanTransId: {} OK!", iThreadId, iRunMode, jsSend.get("fTvanTransId").toString());

            } catch (Exception ex) {
                sendOk = 4;
                strErr = ex.getMessage();
                logger.error("sendToInvoice thread {} - Mode {} send invoice fTvanTransId: {} error: {}!", iThreadId, iRunMode, jsSend.get("fTvanTransId").toString(), ex);
            } finally {
                // Insert sendEinvoiceTransInsert and update status
                this.sendEinvoiceTransInsert(jsKey, jsSend, jsResponse, strErr, sendOk);
                logger.info("sendToInvoice thread {} - Mode {} send invoice fTvanTransId: {} insert sendEinvoiceTransInsert OK!", iThreadId, iRunMode, jsSend.get("fTvanTransId").toString());
            }

        }
        if (iRunMode == MODE_TIME) {
            // update sp_f_s_response_config_update_by_thread
            GlobalUtils.Companion.sleep(ConfigEnum.SER_HANDLE_DELAY_MS.value().toInteger(1000));
            this.responseConfigUpdateByThread(iThreadId);
            logger.info("sendToInvoice thread {} - Mode {} update sp_f_s_response_config_update_by_thread OK!", iThreadId, iRunMode);
        }
    }

    public void sendToInvoice(int iRunMode, int iThreadId, List<JsonObject> lstMsg) {
        int sendOk = 1;
        String strErr = "";
        JsonObject jsSend = null;
        JsonObject jsResponse = null;
        Map<String, String> headerInv;
        JsonObject jsKey = null;
        for (JsonObject jsonObject : lstMsg) {
            try {

                jsKey = JsonObject.newObj();
                jsKey.put("f_id_send_invoice", jsonObject.get("f_id"));
                jsKey.put("f_ref_id", jsonObject.get("f_ref_id"));
                jsKey.put("f_ref_id_tvan", jsonObject.get("f_ref_id_tvan"));
                jsKey.put("f_ref_id_tax", jsonObject.get("f_ref_id_tax"));
                jsKey.put("f_tax_payer_code", jsonObject.get("f_tax_payer_code"));

                // build msg theo chuẩn
                jsSend = this.buildMsgSendInvoice(jsonObject);

                // Send invoice
                headerInv = new HashMap<>();
                headerInv.put("Authorization", "Basic " + Base64.getEncoder().encodeToString(
                        (jsonObject.get("f_username").toString() + ":" + jsonObject.get("f_password").toString()
                        ).getBytes()));

                HttpClient.Response req = HttpClient.post(
                        jsonObject.get("f_timeout").toInteger(),
                        jsonObject.get("f_api_endpoint").toString(), headerInv, jsSend.toStringPretty()
                );
                if (!req.isOk()) {
                    sendOk = 4;
                    strErr = "sendToInvoice Err post data";
                } else {
                    jsResponse = req.bodyJs();
                }
                logger.info("===========WORKER[{}] [Runmode: {}] [Thread: {}]_====fTvanTransId: {} OK!!","sendToInvoice", iRunMode, iThreadId, jsSend.get("fTvanTransId").toString());
            } catch (Exception ex) {
                sendOk = 4;
                strErr = ex.getMessage();
                logger.info("===========WORKER[{}] [Runmode: {}] [Thread: {}]_====fTvanTransId: {} == ERROR: {}!","sendToInvoice", iRunMode, iThreadId, jsSend.get("fTvanTransId").toString(), ex);
            } finally {
                // Insert sendEinvoiceTransInsert and update status
                this.sendEinvoiceTransInsert(jsKey, jsSend, jsResponse, strErr, sendOk);
                logger.info("===========WORKER[{}] [Runmode: {}] [Thread: {}]_====fTvanTransId: {} == insert sendEinvoiceTransInsert OK!!","sendToInvoice", iRunMode, iThreadId, jsSend.get("fTvanTransId").toString());
            }
        }
    }

    public List<JsonObject> getSendInvoiceResponseByStatus(int iRowNum, int iStatus) {
        return this.dbApp.getSendInvoiceResponseByStatus(iRowNum, iStatus);
    }

    public List<JsonObject> getSendInvoiceResponse(int iThreadId, int iStatus) {
        return this.dbApp.getSendInvoiceResponse(iThreadId, iStatus);
    }

    public void sendEinvoiceTransInsert(JsonObject jsKey, JsonObject jsRequest, JsonObject jsResponse, String fErr, int fStatus) {
        this.dbApp.sendEinvoiceTransInsert(jsKey, jsRequest, jsResponse, fErr, fStatus);
    }

    public void responseConfigUpdateByThread(int iThreadId) {
        this.dbApp.responseConfigUpdateByThread(iThreadId);
    }

    public List<JsonObject> getResponseConfig() {
        return this.dbApp.getResponseConfig();
    }

    private JsonObject buildMsgSendInvoice(JsonObject jsInput) {
        JsonObject jsonResponse = JsonObject.build(jsInput.get("f_json_response").toString());

        jsonResponse.put("fTvanTransId", UlidCreator.getUlid().toString());
        jsonResponse.put("fTvanTransDate", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'hh:mm:ss")));
        JsonObject jsData = JsonObject.build(jsonResponse.get("data").toString());
        jsonResponse.put("data", jsData);
        return jsonResponse;
    }
}
