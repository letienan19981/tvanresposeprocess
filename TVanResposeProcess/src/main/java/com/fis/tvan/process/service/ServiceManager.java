package com.fis.tvan.process.service;

import com.fis.tvan.process.base.utils.Partition;
import com.fis.tvan.process.db.DbApp;
import com.fis.tvan.process.impl.ConfigEnum;
import com.fis.tvan.process.service.responseinv.SendToEinvoiceThread;
import com.fis.tvan.process.service.responseinv.handle.SendInvoiceResponseSrv;
import mm.common.utils.GlobalUtils;
import mm.common.utils.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

/**
 * Class quan ly khoi tao va thuc thi cac thread xu ly nghiep vu
 */
@Component
public class ServiceManager {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final int MODE_TIME = 2;
    private final int DB_THREAD_ID = 1;
    private final boolean IS_RUNNING = true;

    @Autowired
    private Environment env;

    @Autowired
    SendInvoiceResponseSrv srv;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private void init(){
        try {
            this.startServiceSendInvoice();
        }catch (Exception e){
            this.logger.error("init worker start have some error! ",e);
            throw e;
        }
    }

    /**
     * Khởi tạo luồng phản hồi cho Invoice
     */
    private void startServiceSendInvoice() {

        int iRunMode = this.env.getProperty("app.run-mode", Integer.class);
        int iSleepTime = this.env.getProperty("app.sleep-time", Integer.class);
        int iRowNum = this.env.getProperty("app.row-num", Integer.class);
        int iStatus = this.env.getProperty("app.f-status", Integer.class);
        int iThreadNum = this.env.getProperty("app.thread-num", Integer.class);
        int iAvailableProcessors = Runtime.getRuntime().availableProcessors();
        iThreadNum = Math.min(iAvailableProcessors, iThreadNum);
        List<JsonObject> lstData;
        List<List<JsonObject>> lstPartitionData;
        SendToEinvoiceThread sendThread;
        Collection<Callable<Void>> tasks;

        while (IS_RUNNING) {
            this.logger.info("WORKER[{}] [Runmode: {}] - [Thread num: {}] I'm fine, start new session zzzzzz", "SendToEinvoiceThread", iRunMode, iThreadNum);
            //Lấy data
            if(iRunMode == MODE_TIME) {
                lstData = this.srv.getSendInvoiceResponse(DB_THREAD_ID, iStatus);
            } else {
                lstData = this.srv.getSendInvoiceResponseByStatus(iRowNum, iStatus);
            }
            if (null == lstData || lstData.size() == 0) {
                logger.info("==========WORKER[{}] [Runmode: {}] - [Thread num: {}]_=========== [EMPTY DATA]!","SendToEinvoiceThread", iRunMode, iThreadNum);
            } else {
                //Chia luồng xử lý, chia nhỏ list đều cho các thread
                lstPartitionData = Partition.ofSize(lstData, (int)Math.ceil((double)lstData.size()/iThreadNum));
                ExecutorService executor = Executors.newFixedThreadPool(iThreadNum);
                tasks = new ArrayList<>();
                for (int iThread = 0; iThread < lstPartitionData.size(); iThread++) {
                    sendThread = this.context.getBean(SendToEinvoiceThread.class);
                    sendThread.initData(iThread, iRunMode, lstPartitionData.get(iThread));
                    SendToEinvoiceThread task = sendThread;
                    tasks.add(task);
                }
                try {
                    // Đợi cho tất cả các thread finished
                    executor.invokeAll(tasks);
                } catch (InterruptedException ex) {
                    this.logger.info("WORKER[{}] [Runmode: {}] - [Thread num: {}] === ERROR: {}", "SendToEinvoiceThread", iRunMode, iThreadNum, ex);
                }

                this.logger.info("WORKER[{}] [Runmode: {}] - [Thread num: {}] Finished !!!!!", "SendToEinvoiceThread", iRunMode, iThreadNum);
            }
            // Update vào bảng f_s_response_config trường hợp iRunMode = 2
            if(iRunMode == MODE_TIME) {
                this.srv.responseConfigUpdateByThread(DB_THREAD_ID);
                logger.info("WORKER[{}] [Runmode: {}] - [Thread num: {}] update sp_f_s_response_config_update_by_thread OK!", "SendToEinvoiceThread", iRunMode, iThreadNum);
            }

            this.logger.info("WORKER[{}] [Runmode: {}] - [Thread num: {}] I'm tired, sleep three second zzzzz!!!!!", "SendToEinvoiceThread", iRunMode, iThreadNum);
            GlobalUtils.Companion.sleep(ConfigEnum.SER_HANDLE_DELAY_MS.value().toInteger(iSleepTime));
        }
    }
}
