package com.fis.tvan.process.impl;

import com.fis.tvan.process.base.config.ProviderGlobal;

public enum AppErrorEnum {
    success(0)
    ,system_busy(-1)
    , xml_invalid_format(1)
    ,input_is_not_base64(2)
    ,input_unknown_fields(3)
    ,message_function_not_support(4)
    , tax_code_is_null_or_empty(5)
    ,input_is_required(6)
    ;

    private int code;
    private String groupName = null;
    AppErrorEnum(final String groupName, final int code){
        this.groupName = groupName;
        this.code = code;
    }

    AppErrorEnum(final int code){
        this.code = code;
    }


    public int code(){
        return this.code;
    }

    public String value(){
        return String.valueOf(this.code);
    }

    public String toStr(boolean isEn){
       return ProviderGlobal.appError.get(this.groupName, this.code, this.code + " "+this.toString(), isEn);
    }

    public String toStrFormat(boolean isEn, Object...params){
        String val = this.toStr(isEn);
        try {
            return String.format(val,params);
        }catch (Exception e){
            return val;
        }
    }

}
