package com.fis.tvan.process.base.config;

import com.fis.tvan.base.common.AppErrorLoad;
import com.fis.tvan.base.common.ConfigLoad;
import com.fis.tvan.base.common.TVanUtils;
import mm.common.utils.JXmlObject;
import mm.common.utils.db.DbPostgresPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ProviderGlobal {
    public static ConfigLoad configsApp;
    public static AppErrorLoad appError;
    public static TVanUtils tVanUtils;


    @Autowired
    private void init(ConfigLoad config, AppErrorLoad appErr){
            ProviderGlobal.configsApp = config;
            ProviderGlobal.appError = appErr;
            ProviderGlobal.tVanUtils = new TVanUtils(ProviderGlobal.configsApp);
    }
}
