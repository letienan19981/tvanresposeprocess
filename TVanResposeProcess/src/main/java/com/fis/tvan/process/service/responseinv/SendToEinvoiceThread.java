package com.fis.tvan.process.service.responseinv;

import com.fis.tvan.process.db.DbApp;
import com.fis.tvan.process.impl.ConfigEnum;
import com.fis.tvan.process.service.responseinv.handle.SendInvoiceResponseSrv;
import mm.common.utils.GlobalUtils;
import mm.common.utils.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

@Component
@Scope("prototype")
public class SendToEinvoiceThread implements Callable {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private int iThreadId = 0;
    private int iRunMode = 1;
    private List<JsonObject> lstData;

    @Autowired
    SendInvoiceResponseSrv srv;

    public void initData(int iThreadId, int iRunMode) {
        this.iThreadId = iThreadId;
        this.iRunMode = iRunMode;
    }

    public void initData(int iThreadId, int iRunMode, List<JsonObject> lstData) {
        this.iThreadId = iThreadId;
        this.iRunMode = iRunMode;
        this.lstData = lstData;
    }

    @Override
    public String call() {
        this.logger.info("===========Start _WORKER[{}] [Runmode: {}] [Thread: {}]_===========", "SendToEinvoiceThread", this.iRunMode, this.iThreadId);
        this.srv.sendToInvoice(this.iRunMode, this.iThreadId, this.lstData);
        // this.latch.countDown();
        return  null;
    }
}
