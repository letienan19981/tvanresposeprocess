package com.fis.tvan.process.base.config;

import com.fis.tvan.base.common.AppErrorLoad;
import com.fis.tvan.base.common.ConfigLoad;
import com.fis.tvan.process.base.Const;
import mm.common.utils.JsonObject;
import mm.common.utils.db.DbConfig;
import mm.common.utils.db.DbPostgresPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class BeanConfig {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private Environment env;

    @Bean
    private DbPostgresPool dbPool(Environment env){
        String tagDb = "datasource.db-app";
        return this.buildDbSource(tagDb);
    }

    private DbPostgresPool buildDbSource(String tagDb){
        DbConfig config = new DbConfig();
        String tagTmp="";
        try {
            tagTmp = tagDb+"."+"driver-class-name";
            if(this.env.containsProperty(tagTmp)){
                config.setDriverClassName(this.env.getProperty(tagTmp));
            }

            tagTmp = tagDb+"."+"jdbc-url";
            if(this.env.containsProperty(tagTmp)){
                config.setJdbcUrl(this.env.getProperty(tagTmp));
            }

            tagTmp = tagDb+"."+"user-name";
            if(this.env.containsProperty(tagTmp)){
                config.setUsername(this.env.getProperty(tagTmp));
            }

            tagTmp = tagDb+"."+"pass-word";
            if(this.env.containsProperty(tagTmp)){
                config.setPassword(this.env.getProperty(tagTmp));
            }

            tagTmp = tagDb+"."+"minimum-idle";
            if(this.env.containsProperty(tagTmp)){
                config.setMinimumIdle(this.env.getProperty(tagTmp, Integer.class));
            }

            tagTmp = tagDb+"."+"maximum-pool-size";
            if(this.env.containsProperty(tagTmp)){
                config.setMaximumPoolSize(this.env.getProperty(tagTmp, Integer.class));
            }

            tagTmp = tagDb+"."+"pool-name";
            if(this.env.containsProperty(tagTmp)){
                config.setPoolName(this.env.getProperty(tagTmp));
            }

            tagTmp = tagDb+"."+"connection-timeout";
            if(this.env.containsProperty(tagTmp)){
                config.setConnectionTimeout(this.env.getProperty(tagTmp,Long.class));
            }

            tagTmp = tagDb+"."+"max-life-time";
            if(this.env.containsProperty(tagTmp)){
                config.setMaxLifetime(this.env.getProperty(tagTmp, Long.class));
            }

            tagTmp = tagDb+"."+"cache-prep-stmts";
            if(this.env.containsProperty(tagTmp)){
                config.setCachePrepStmts(this.env.getProperty(tagTmp, Boolean.class));
            }

            tagTmp = tagDb+"."+"prep-stmt-cache-size";
            if(this.env.containsProperty(tagTmp)){
                config.setPrepStmtCacheSize(this.env.getProperty(tagTmp, Integer.class));
            }

            tagTmp = tagDb+"."+"prep-stmt-cache-sql-limit";
            if(this.env.containsProperty(tagTmp)){
                config.setPrepStmtCacheSqlLimit(this.env.getProperty(tagTmp, Integer.class));
            }

            tagTmp = tagDb+"."+"max-row-get";
            if(this.env.containsProperty(tagTmp)){
                config.setMaxRowGet(this.env.getProperty(tagTmp, Integer.class));
            }
            JsonObject jsLog= JsonObject.build(config);
            jsLog.remove("username");
            jsLog.remove("password");
            this.logger.info("***********Create connect***********\n{}",jsLog.toStringPretty());
            DbPostgresPool dbNew = new DbPostgresPool(config);
            return dbNew;
        }catch (Exception e){
            this.logger.error("Can not connect database!",e);
        }
        return null;
    }

}
