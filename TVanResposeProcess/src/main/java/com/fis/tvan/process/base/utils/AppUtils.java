package com.fis.tvan.process.base.utils;

import com.fasterxml.jackson.databind.JsonNode;
import mm.common.utils.JsUtils;
import mm.common.utils.JsonObject;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.core.env.Environment;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.*;

public class AppUtils {
    public static void rebuildMsgResponse(JsonObject jsData){
        Map.Entry<String, JsonNode> row;
        JsonObject rowValue;
        for(JsonNode node : jsData.toJsonNode()){
            Iterator<Map.Entry<String, JsonNode>> item = node.fields();
            while (item.hasNext()){
                row = item.next();
                rowValue = JsonObject.build(JsUtils.toString(row.getValue()));
                if(rowValue.isJson()){
                    row.setValue(rowValue.toJsonNode());
                }
            }
        }
    }

    public static String sha256ToBase64(String data){
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(data.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(hash);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * Load config trong spring va set vao class voi field co dinh dang xxx__x__xxx
     * Luu y: viec load config nhu the nay chi phu hop voi cac config co kieu du lieu nguyen thuy. neu object thi khong nen su dung
     * @param env
     * @param classMap
     * @param <T>
     */
    public static<T>  void loadEnvToConfigMap(Environment env, Class<T> classMap){
        try{
            String fieldName;
            for(Field field : classMap.getDeclaredFields()){
                if(Modifier.isStatic(field.getModifiers())){
                    fieldName = field.getName().replaceAll("__",".").replaceAll("_","-");
                    if(env.containsProperty(fieldName)){
                        field.setAccessible(true);
                        field.set(null, env.getProperty(fieldName,field.getType()));
                    }
                }
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }

    public static String splitError(String text, String beginTag, String endTag, String defVal){
        if (null == text || null == beginTag || null == endTag) {
            return defVal;
        }
        int idxSt = beginTag.isEmpty()? 0 : text.indexOf(beginTag);
        int idxEnd = endTag.isEmpty()? text.length() : text.indexOf(endTag);
        if(idxSt<0 || idxEnd<0 || idxSt >= idxEnd){
            return defVal;
        }
        return text.substring(idxSt+beginTag.length(), idxEnd);
    }

    public static String splitTagData(String text, String beginTag, String endTag, String defVal){
        if (null == text || null == beginTag) {
            return defVal;
        }
        int idxSt = text.indexOf(beginTag);
        int idxEnd = text.lastIndexOf(endTag);
        if(idxSt<0 || idxEnd<0 || idxSt >= idxEnd){
            return defVal;
        }
        return text.substring(idxSt+beginTag.length(), idxEnd);
    }

    public static String splitTagDataXml(String text, String tag, String defVal){
        return splitTagData(text,"<"+tag+">", "</"+tag+">", defVal);
    }

    public static String getXmlResult(String message, String tagResult) {
        try{
            return StringEscapeUtils.unescapeXml(
                    AppUtils.splitTagDataXml(message, tagResult, message)
            );
        }catch (Exception e){
            System.out.println(e);
        }
        return message;
    }


    public static void main(String[] args) {
        System.out.println("=====> "+ Instant.now().toString() );
    }
}
