package com.fis.tvan.process.db;

import com.fis.tvan.process.impl.ActInfoDto;
import com.fis.tvan.process.impl.AppErrorEnum;
import mm.common.utils.JsonObject;
import mm.common.utils.StringUtils;
import mm.common.utils.db.DbPostgresPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
public class DbApp {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    // private Logger dbLogError = LoggerFactory.getLogger("dbLogger");

    @Autowired()
    private DbPostgresPool db;

    /*
    * Get data json send_invoice
    *
    * */
    public List<JsonObject> getSendInvoiceResponse(int threadId, int status){
        List<JsonObject> result = this.db.query("sp_f_tvan_send_einvoice_select_for_response",
                threadId,
                null,
                null,
                100,
                status
            ).asList();
        return result;
    }

    public List<JsonObject> getSendInvoiceResponseByStatus(int rowNum, int status){
        List<JsonObject> result = this.db.query("sp_f_tvan_send_einvoice_select_for_response",
                rowNum,
                status
        ).asList();
        return result;
    }

    public List<JsonObject> getResponseConfig(){
        List<JsonObject> result = this.db.query("sp_f_s_response_config_select"
        ).asList();
        return result;
    }

    public HashMap<String, JsonObject> getResponseEnpoint(){
        HashMap<String, JsonObject> mapEnpoint = new HashMap<>();
        List<JsonObject> result = this.db.query("sp_f_s_response_endpoint_select"
        ).asList();
        for(JsonObject jsonObject: result) {
            mapEnpoint.put(jsonObject.get("f_tax_payer_code").toString(), jsonObject);
        }
        return mapEnpoint;
    }

    /*
        sp_f_s_response_config_update_by_thread
     */
    public boolean responseConfigUpdateByThread(int threadId) {
        long st = System.currentTimeMillis();
        try {
            this.db.execute("sp_f_s_response_config_update_by_thread",
                    threadId // IN pf_thread_id int4
            );
            return true;
        } catch (Exception e) {
            this.logger.error("[DB] responseConfigUpdateByThread error: {}", "fErr", e);
//            this.dbLogError.error(fErr);
        } finally {
            this.logger.info("[DB] responseConfigUpdateByThread complete: {} ms", StringUtils.Companion.getTimeToLog(st));
        }
        return false;
    }

    /*
     * Insert data to request, resposne
     * insert khi đã nhận được response từ insert
     * gặp lỗi thì ghi log
     *
     * */

    public boolean sendEinvoiceTransInsert(JsonObject jsKey, JsonObject jsRequest, JsonObject jsResponse, String fErr, int fStatus) {

        long st = System.currentTimeMillis();
        try {
            this.db.execute("sp_f_tvan_send_einvoice_trans_insert",
                    null, // IN pf_id character varying,
                    null, // IN pf_date timestamp without time zone,
                    jsRequest.get("fTvanTransId").toString(),
                    jsRequest.get("fTvanTransDate").toString(),
                    (!jsResponse.fields().containsKey("fEinvoiceTransId") ? jsRequest.get("fTvanTransId").toString():jsResponse.get("fEinvoiceTransId").toString()),
                    (!jsResponse.fields().containsKey("fEinvoiceTransDate") ? jsRequest.get("fTvanTransDate").toString():jsResponse.get("fEinvoiceTransDate").toString()),
                    jsRequest.get("senderCode").toString(),
                    jsRequest.get("senderName").toString(),
                    jsRequest.get("receiverCode").toString(),
                    jsRequest.get("receiverName").toString(),
                    jsRequest.toStringPretty(), //pf_tvan_json
                    jsResponse.toStringPretty(), //pf_einvoice_json
                    jsKey.get("f_id_send_invoice").toString(), // f_id_send_invoice,
                    jsKey.get("f_ref_id").toString(), // f_ref_id,
                    jsKey.get("f_ref_id_tvan").toString(), // f_ref_id_tvan,
                    jsKey.get("f_ref_id_tax").toString(), // f_ref_id_tax
                    null, //f_source_data
                    null, //f_created_date
                    null, //f_updated_date
                    jsKey.get("f_tax_payer_code").toString(), //f_enterprise_code
                    fStatus, //f_status
                    String.valueOf(fStatus), //f_err_code
                    fErr //f_err_msg
            );
            return true;
        } catch (Exception e) {
            this.logger.error("[DB] sendEinvoiceTransInsert error: {}", fErr, e);

        } finally {
            this.logger.info("[DB] sendEinvoiceTransInsert complete: {} ms", StringUtils.Companion.getTimeToLog(st));
        }
        return false;
    }

}
