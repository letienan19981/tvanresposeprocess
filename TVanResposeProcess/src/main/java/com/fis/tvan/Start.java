package com.fis.tvan;

import mm.common.utils.FileUtils;
import mm.common.utils.FlagUtils;
import mm.common.utils.GlobalUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = {"com.fis.tvan"})
@SpringBootApplication
public class Start {
    private static final String spring_config_location = "--spring.config.location";
    private static final String log_config = "--logger.config";

    public static void main(String[] args) {
        GlobalUtils.Companion.configLogger(FlagUtils.Companion.getFlag(log_config,"etc/log.xml"));
        GlobalUtils.Companion.argsLoad(args);
        FlagUtils.Companion.extendFlagSkipExist(spring_config_location, FileUtils.Companion.userDir("etc/application.yml"));
        SpringApplication.run(Start.class, FlagUtils.Companion.getArgs());
        // ApplicationContext applicationContext = SpringApplication.run(Start.class, FlagUtils.Companion.getArgs());

    }

}