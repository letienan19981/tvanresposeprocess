import mm.common.utils.JsonObject;
import mm.common.utils.XmlParse;
import org.json.*;

import java.util.Collections;
import java.util.regex.Pattern;

public class TestCode {
    static String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
            "<MetadataModel zzz=\"123123\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "  <Profile>\n" +
            "    <ProfileCode>HS0001</ProfileCode>\n" +
            "    <ProfileDate>2021-11-22T19:40:34.6184017+07:00</ProfileDate>\n" +
            "    <ObjectCode>HS01</ObjectCode>\n" +
            "    <Address>Phạm Văn Đồng</Address>\n" +
            "    <IsAES>false</IsAES>\n" +
            "    <CreateBy>soho</CreateBy>\n" +
            "    <CreateDate>2021-11-22T19:44:55.9044715+07:00</CreateDate>\n" +
            "    <EndTime>2021-11-22T19:44:55.9044715+07:00</EndTime>\n" +
            "    <StartTime>2021-11-22T19:41:47.1104849+07:00</StartTime>\n" +
            "    <IsVideo>true</IsVideo>\n" +
            "    <ProfileNotes>\n" +
            "      <MetadataProfileNoteModel>\n" +
            "        <Name>Luật sư đến</Name>\n" +
            "        <Second>70.55624499999999</Second>\n" +
            "      </MetadataProfileNoteModel>\n" +
            "    </ProfileNotes>\n" +
            "    <Cadres>\n" +
            "      <MetadataCadreModel>\n" +
            "        <Index>1</Index>\n" +
            "        <Name>Nuyễn Văn A</Name>\n" +
            "        <Organization />\n" +
            "      </MetadataCadreModel>\n" +
            "    </Cadres>\n" +
            "    <Objects>\n" +
            "      <MetadataObjectModel>\n" +
            "        <Index>0</Index>\n" +
            "        <Name>Lê Văn C</Name>\n" +
            "        <DateOfBirth xsi:nil=\"true\" />\n" +
            "        <Address />\n" +
            "        <OtherName />\n" +
            "        <PermanentResidence />\n" +
            "        <Gender />\n" +
            "      </MetadataObjectModel>\n" +
            "    </Objects>\n" +
            "    <Lawyers>\n" +
            "      <MetadataLawyerModel>\n" +
            "        <Index>1</Index>\n" +
            "        <Name>Đào Văn C</Name>\n" +
            "        <Organization />\n" +
            "      </MetadataLawyerModel>\n" +
            "    </Lawyers>\n" +
            "    <FileMedias>\n" +
            "      <MetadataFileMediaModel>\n" +
            "        <Index>0</Index>\n" +
            "        <FileName>20211122_193928_final_4.5GB_0000.mp4</FileName>\n" +
            "        <FilePath>C:\\records\\20211122_193928-Encryt\\20211122_193928_final_4.5GB_0000.mp4</FilePath>\n" +
            "        <Duration>98.887</Duration>\n" +
            "        <CreateDate>2021-11-22T19:44:55.1973638+07:00</CreateDate>\n" +
            "        <SHA256>4571C6E62EC917F69BDA53276BB31C195017FE18B0C52780D31CA8A7B69BD9DD</SHA256>\n" +
            "      </MetadataFileMediaModel>\n" +
            "    </FileMedias>\n" +
            "  </Profile>\n" +
            "  <Notes>\n" +
            "    <NoteModel>\n" +
            "      <Name>Luật sư đến</Name>\n" +
            "      <Second>70.55624499999999</Second>\n" +
            "    </NoteModel>\n" +
            "    <NoteModel>\n" +
            "      <Name>Luật sư đến2222</Name>\n" +
            "      <Second>70.55624121239</Second>\n" +
            "    </NoteModel>\n" +
            "     <Second2>70.55624499999999</Second2>\n" +
            "  </Notes>\n" +
            "  <Duration>98.887</Duration>\n" +
            "  <Files>\n" +
            "    <MetadataFileModel>\n" +
            "      <FileName>20211122_193928_final_4.5GB_0000.mp4</FileName>\n" +
            "      <Duration>98.887</Duration>\n" +
            "      <StartOffset>0</StartOffset>\n" +
            "      <EndOfffset>98.887</EndOfffset>\n" +
            "      <SHA256>4571C6E62EC917F69BDA53276BB31C195017FE18B0C52780D31CA8A7B69BD9DD</SHA256>\n" +
            "    </MetadataFileModel>\n" +
            "  </Files>\n" +
            "</MetadataModel>";

    static String xml2 ="<TKhai>\n" +
            "\t\t\t<DLTKhai>\n" +
            "\t\t\t\t<TTChung>\n" +
            "\t\t\t\t\t<PBan>2.0.0</PBan>\n" +
            "\t\t\t\t\t<MSo>01/ĐKTĐ-HĐĐT</MSo>\n" +
            "\t\t\t\t\t<Ten>Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử</Ten>\n" +
            "\t\t\t\t\t<HThuc>1</HThuc> <!-- 1: đăng ký mới, 2: thay đổi thông tin -->\n" +
            "\t\t\t\t\t<TNNT>Vinmart</TNNT>\n" +
            "\t\t\t\t\t<MST>0104918404</MST>\n" +
            "\t\t\t\t\t<CQTQLy>Chi cục thuế Quận Hoàng Mai</CQTQLy>\n" +
            "\t\t\t\t\t<MCQTQLy>10108</MCQTQLy>\n" +
            "\t\t\t\t\t<NLHe>NGUYỄN THỊ DUNG</NLHe>\n" +
            "\t\t\t\t\t<DCLHe>Quận Hoàng Mai, Hà Nội</DCLHe>\n" +
            "\t\t\t\t\t<DCTDTu>dungnguyentran@gmail.com</DCTDTu>\n" +
            "\t\t\t\t\t<DTLHe>0394552327</DTLHe>\n" +
            "\t\t\t\t\t<DDanh>Hà Nội</DDanh>\n" +
            "\t\t\t\t\t<NLap>2021-11-11</NLap>\n" +
            "\t\t\t\t</TTChung>\t\t\n" +
            "\t\t\t\t<NDTKhai>\n" +
            "\t\t\t\t\t<HTHDon>\n" +
            "\t\t\t\t\t\t<CMa>1</CMa>\n" +
            "\t\t\t\t\t\t<KCMa>0</KCMa>\n" +
            "\t\t\t\t\t</HTHDon>\n" +
            "\t\t\t\t\t<HTGDLHDDT>\n" +
            "\t\t\t\t\t\t<NNTDBKKhan>0</NNTDBKKhan>\n" +
            "\t\t\t\t\t\t<NNTKTDNUBND>0</NNTKTDNUBND>\n" +
            "\t\t\t\t\t\t<CDLTTDCQT>0</CDLTTDCQT>\n" +
            "\t\t\t\t\t\t<CDLQTCTN>0</CDLQTCTN>\n" +
            "\t\t\t\t\t</HTGDLHDDT>\n" +
            "\t\t\t\t\t<PThuc>\n" +
            "\t\t\t\t\t\t<CDDu>1</CDDu>\n" +
            "\t\t\t\t\t\t<CBTHop>0</CBTHop>\n" +
            "\t\t\t\t\t</PThuc>\n" +
            "\t\t\t\t\t<LHDSDung>\n" +
            "\t\t\t\t\t\t<HDGTGT>1</HDGTGT>\n" +
            "\t\t\t\t\t\t<HDBHang>1</HDBHang>\n" +
            "\t\t\t\t\t\t<HDBTSCong>0</HDBTSCong>\n" +
            "\t\t\t\t\t\t<HDBHDTQGia>0</HDBHDTQGia>\n" +
            "\t\t\t\t\t\t<HDKhac>0</HDKhac>\n" +
            "\t\t\t\t\t\t<CTu>1</CTu>\n" +
            "\t\t\t\t\t</LHDSDung>\n" +
            "\t\t\t\t\t<DSCTSSDung>\n" +
            "\t\t\t\t\t\t<CTS>\n" +
            "\t\t\t\t\t\t\t<STT>1</STT>\n" +
            "\t\t\t\t\t\t\t<TTChuc>Fpt-CA</TTChuc>\n" +
            "\t\t\t\t\t\t\t<Seri>0123456789</Seri>\n" +
            "\t\t\t\t\t\t\t<TNgay>2020-05-01T00:00:00</TNgay>\n" +
            "\t\t\t\t\t\t\t<DNgay>2022-10-01T00:00:00</DNgay>\n" +
            "\t\t\t\t\t\t\t<HThuc>1</HThuc> <!-- 1: đăng ký mới, 2: gia hạn, 3: Ngừng sử dụng -->\n" +
            "\t\t\t\t\t\t</CTS>\n" +
            "\t\t\t\t\t\t<CTS>\n" +
            "\t\t\t\t\t\t\t<STT>1</STT>\n" +
            "\t\t\t\t\t\t\t<TTChuc>misa-CA</TTChuc>\n" +
            "\t\t\t\t\t\t\t<Seri>0123456788</Seri>\n" +
            "\t\t\t\t\t\t\t<TNgay>2020-05-01T00:00:00</TNgay>\n" +
            "\t\t\t\t\t\t\t<DNgay>2022-10-01T00:00:00</DNgay>\n" +
            "\t\t\t\t\t\t\t<HThuc>1</HThuc>\n" +
            "\t\t\t\t\t\t</CTS>\n" +
            "\t\t\t\t\t</DSCTSSDung>\n" +
            "\t\t\t\t</NDTKhai>\t\n" +
            "\t\t\t</DLTKhai>\t\n" +
            "\t\t\t<DSCKS>\n" +
            "\t\t\t\t<NNT>\n" +
            "\t\t\t\t<Signature>DFJSDGHFSDHFHSDUIFH</Signature>\n" +
            "\t\t\t\t</NNT>\n" +
            "\t\t\t</DSCKS>\n" +
            "\t\t</TKhai>";

    public static void main(String[] args) {
        try {
//            xml2 = "<a>123</a>";
//            XMLParserConfiguration config = new XMLParserConfiguration();
//            config = config.withKeepStrings(true).withConvertNilAttributeToNull(true).withcDataTagName("value");
//            JSONObject xmlJSONObj = XML.toJSONObject(xml2, config);
//            String jsonPrettyPrintString = xmlJSONObj.toString(4);
            String jsonPrettyPrintString = XmlParse.toJsonObject(xml2).toStringPretty();
            System.out.println(jsonPrettyPrintString);
            System.out.println("\n\n");
        } catch (JSONException je) {
            System.out.println(je.toString());
        }
    }
}
