#!/bin/bash
#Lay thong tin cac pid chay voi lenh java
APP_CMD=java
APP_NAME=.jar
#Lay thong tin folder dang lam viec
WORKING_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#Chuyen den folder dang chua file batch
cd $WORKING_DIR



#Lay thong tin ung dung
APP_NAME=$(ls|grep "$APP_NAME"$)
echo ""
echo "    || //    \\\\  //   "
echo "    ||//      \\\\//    "
echo "    ||\\\\      //\\\\    "
echo "    || \\\\    //  \\\\   "
echo "    ||  \\\\[]//    \\\\  "
echo ""
echo "- [App running]: "${APP_NAME}
echo "- [Working dir]: "${WORKING_DIR}
echo "..."
# exit
# echo ""

#xuat ra danh sach cac PID dang chay voi APP_CMD
PID_ARR=$(ps -C ${APP_CMD} -o pid)

echo "PID check: "$PID_ARR

#kiem tra xem co process nao ma khong duoc quyen truy van khong
for i in $(echo $PID_ARR | tr " " "\n")
do
  if [[ $i == "PID" ]]
  then
	continue
  fi

  # process
  PID_DETAIL=$(pwdx $i)
  
  #neu khong co quyen truy cap pid. can yeu cau quyen truy cap
  if [[ $PID_DETAIL == "" ]]
	then
	    echo "warning!"
		echo "- You need permission to access pid: "$i
		echo "===================================================================="
		exit
  fi 
done

TOTAL_PROCESS=0
#cat ra cac PID va thuc hien truy van lay thong tin thu muc chua process
for i in $(echo $PID_ARR | tr " " "\n")
do
  # process
  PID_DETAIL=$(pwdx $i)
  
  if [[ $PID_DETAIL == *$WORKING_DIR ]] ;
  then
    TOTAL_PROCESS=$(expr $TOTAL_PROCESS + 1)
	echo "---------------KILL PROCESS ["$i"]---------------"
	echo ":: APP :: "$APP_NAME
	echo ":: DIR :: "$PID_DETAIL
	echo ":: PID :: "$(ps -f -p $i|grep $i)
	echo "waiting..."$(kill -9 $i)
	sleep 1
	
	RECHECK=$(ps -f -p $i|grep $i)
	if [[ $RECHECK == "" ]]
	  then
		echo ":: KILL COMPLETE :: "$PID_DETAIL"/"$APP_NAME
	  else
		echo ":: KILL FAILED :: "$RECHECK
	fi
	
	echo "--------------- END ["$i"]-----------------------"
	# exit
  fi
done
echo ""
echo "TOTAL PROCESS: "$TOTAL_PROCESS
echo "===================================================================="