#!/bin/bash

#ten service
SERVICE_NAME="TVanResposeProcess"

ACTION="unknown"
for param in "$@" 
do
    ACTION=$param
	break
done

if [[ $ACTION != "create" && $ACTION != "stop" && $ACTION != "start" && $ACTION != "restart" ]]
then
	echo "warning!"
	echo "- Only allow action: create | stop | start | restart"
	echo "===================================================================="
fi


if [[ $SERVICE_NAME == "" ]]
	then
		echo "warning!"
		echo "- SERVICE_NAME can not be empty!"
		echo "===================================================================="
		exit
fi

SERVICE_NAME=$SERVICE_NAME".service"

echo ""
echo "    || //    \\\\  //   "
echo "    ||//      \\\\//    "
echo "    ||\\\\      //\\\\    "
echo "    || \\\\    //  \\\\   "
echo "    ||  \\\\[]//    \\\\  "
echo "                      [Action]: "${ACTION}" > "$SERVICE_NAME
echo ""
echo "..."
#Lay thong tin folder dang lam viec
WORKING_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

#Chuyen den folder dang chua file batch
cd /etc/systemd/system
CONFIG_DIR=$(pwd)

if [[ $CONFIG_DIR != "/etc/systemd/system" ]]
	then
		echo "warning!"
		echo "- Working dir: "$CONFIG_DIR
		echo "- You need permission to access: /etc/systemd/system"
		echo "===================================================================="
		exit
fi

echo "- Switch Working: "$CONFIG_DIR


#tao service
if [ $ACTION == "create" ]
then
	if [ -e $SERVICE_NAME ]
	then
			echo "warning!"
			echo "- Service already exist: "$SERVICE_NAME
			echo "--------------------------------------------------------------------"
			tail $SERVICE_NAME
			echo "===================================================================="
			exit
	fi
	
	echo "[Unit]" > $SERVICE_NAME
	echo "Description=$SERVICE_NAME" >> $SERVICE_NAME
	echo "" >> $SERVICE_NAME
	echo "[Service]" >> $SERVICE_NAME
	echo "ExecStart=$WORKING_DIR/start.sh" >> $SERVICE_NAME
	echo "" >> $SERVICE_NAME
	echo "[Install]" >> $SERVICE_NAME
	echo "WantedBy=default.target" >> $SERVICE_NAME
	sleep 1	
	
	if [ -e $SERVICE_NAME ]
		then
		    sudo systemctl daemon-reload
			systemctl enable $SERVICE_NAME
			echo "create service [$SERVICE_NAME] success!"
			echo "--------------------------------------------------------------------"
			tail $SERVICE_NAME
			echo "===================================================================="
			exit
		fi
	
	echo "warning!"
	echo "- Can not create service: "$SERVICE_NAME
	echo "===================================================================="	
		
	exit
fi



if [[ $ACTION == "stop"  ||  $ACTION == "restart" ]]
then
	IS_STARTING=$(systemctl is-active --quiet $SERVICE_NAME && echo Service is running)
	if [[ $IS_STARTING == "" ]]
	then
		IS_STARTING="no running"
	fi
	
	echo ""
	echo ""
	echo "Acvie status: "$SERVICE_NAME" ----------> "$IS_STARTING
	echo "***"
	
	if [ -e $SERVICE_NAME ]
	then			
		#neu service khong running thi bo qua
		if [[ $IS_STARTING == "no running" ]]
		then
				echo "warning!"
				echo "- Service is not active: "$SERVICE_NAME
				echo "==============================_STOP_======================================"
				sleep 1
				systemctl -l status $SERVICE_NAME
				echo "============================_STOP-END_===================================="
		else
				systemctl stop $SERVICE_NAME
				echo "waiting stop service..."$SERVICE_NAME
				sleep 1			
				echo "==============================_STOP_======================================"
				systemctl status $SERVICE_NAME
				echo "============================_STOP-END_===================================="
		fi		
	else
			echo "warning!"
			echo "- Service not exist: "$SERVICE_NAME
			echo "============================_STOP-END_===================================="
			exit		
	fi

fi



if [[ $ACTION == "start" || $ACTION == "restart" ]]
then
	IS_STARTING=$(systemctl is-active --quiet $SERVICE_NAME && echo Service is running)
	if [[ $IS_STARTING == "" ]]
	then
		IS_STARTING="no running"
	fi
	
	echo ""
	echo ""
	echo "Acvie status: "$SERVICE_NAME" ----------> "$IS_STARTING
	echo "***"

	if [ -e $SERVICE_NAME ]
	then					
			#neu service running thi bo qua
			if [[ $IS_STARTING != "no running" ]]
			then
					echo "warning!"
					echo "- Service is actived: "$SERVICE_NAME
					sleep 1
					echo "==============================_START_======================================"
					systemctl -l status $SERVICE_NAME
					echo "============================_START-END_===================================="	
			else		
					systemctl start $SERVICE_NAME
					echo "waiting start service..."$SERVICE_NAME
					echo "==============================_START_======================================"
					sleep 1			
					systemctl -l status $SERVICE_NAME
					echo "============================_START-END_===================================="	
			fi
	else
			echo "warning!"
			echo "- Service not exist: "$SERVICE_NAME
			echo "============================_START-END_===================================="
			exit		
	fi
fi



